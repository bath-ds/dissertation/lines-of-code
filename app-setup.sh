#!/bin/bash 

# setup RN project
name=${1:-app}
react-native init $name --version react-native@0.60.4
cd $name
npm i
npm i -S lodash.get@^4.4.2 native-base@^2.12.2 react-native-gesture-handler@^1.3.0 react-navigation@^3.11.1
react-native link
# gesture handler fix
# https://github.com/kmagiera/react-native-gesture-handler/issues/494
cd ios
pod install
cd ..
