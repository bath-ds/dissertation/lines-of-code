# Lines of Code

A system of applications with the goal of transforming a low-fidelity user interface sketch into compilable source code.

## Prerequisites

Below are the list of prerequisite software that must be installed prior to installing or running Lines of Code.

- [Python 3.7](https://www.python.org/)
- [virtualenv](https://virtualenv.pypa.io/en/latest/installation/)
- [Tensorflow Object Detection API](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md)
- [Node.js](https://nodejs.org/en/)

## Installation

Run the `./install.sh` script. The script will clone the [`sketch-to-dsl`](https://gitlab.com/bath-ds/dissertation/sketch-to-dsl) and [`dsl-to-code`](https://gitlab.com/bath-ds/dissertation/dsl-to-code) repositories, and install all their respective requirements with `pip` and `npm`.

### (Optional) React Native Project Setup Script

A helper script (`app-setup.sh`) can be executed to generate a React Native project with additional dependencies required by the project. [React Native](https://facebook.github.io/react-native/) must be installed prior to running the helper script.

## Running the Application

Run the `run.sh` script with the following parameters.

    ./run.sh <input_image> <output_directory> [pages]

    input_image:
      Full path to the input image.

    output_directory:
      Full path to the output directory where the source code will be generated. Typically, this would be the root directory of a React Native project.

    pages (optional):
      A non-zero positive integer that specifies how many pages are in the input image. Supplying a valid argument to this parameter forces the application to use KMeans clustering instead of the default DBSCAN algorithm.

## Examples

The [`sketch-to-dsl`](https://gitlab.com/bath-ds/dissertation/sketch-to-dsl) repository contains several example images. The following example runs should work out of the box.

    ./run.sh sketch-to-dsl/input/00-test01.jpg app/

<!-- TODO: citation -->
