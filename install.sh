#!/bin/bash 

# install sketch to dsl
git clone git@gitlab.com:bath-ds/dissertation/sketch-to-dsl.git
cd sketch-to-dsl
virtualenv -p python3 env
. env/bin/activate
pip install -r requirements.txt
deactivate
cd ..

# install dsl to code
git clone git@gitlab.com:bath-ds/dissertation/dsl-to-code.git
cd dsl-to-code
npm install
cd ..
