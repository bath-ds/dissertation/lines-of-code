#!/bin/bash 

# clean output folders
rm -rf dsl-to-code/input/*

# run
sketch-to-dsl/env/bin/python sketch-to-dsl/main.py -p=${3:-0} $1 ./dsl-to-code/input && \
node dsl-to-code/index.js dsl-to-code/input $2

# examples
# ./run.sh ./sketch-to-dsl/input/00-test01.jpg app